// vuex structure

// Path: client/src/store/modules/notes.js

import axios from "../../axios";
// import config from "../../config";

const state = {
  notes: [],
  note: {},
  isLoading: false,
};

const mutations = {
  setNotes(state, notes) {
    state.notes = notes;
  },

  setNote(state, note) {
    state.note = note;
  },

  addNote(state, note) {
    state.notes.push(note);
  },

  updateNote(state, note) {
    const index = state.notes.findIndex((n) => n.id === note.id);
    if (index !== -1) {
      state.notes.splice(index, 1, note);
    }
  },

  deleteNote(state, noteId) {
    const index = state.notes.findIndex((n) => n.id === noteId);
    if (index !== -1) {
      state.notes.splice(index, 1);
    }
  },

  setLoading(state, isLoading) {
    state.isLoading = isLoading;
  },
};

const actions = {
  async fetchNotes({ commit }) {
    commit("setLoading", true);
    try {
      const response = await axios.get("/api/notes");
      commit("setNotes", response.data.data);
    } catch (error) {
    } finally {
      commit("setLoading", false);
    }
  },

  async fetchNote({ commit }, noteId) {
    commit("setLoading", true);
    try {
      const response = await axios.get(`/api/notes/${noteId}`);
      commit("setNote", response.data);
    } catch (error) {
    } finally {
      commit("setLoading", false);
    }
  },

  async createNote({ commit }, note) {
    commit("setLoading", true);
    try {
      const response = await axios.post("/api/notes", note);
      commit("addNote", response.data);
    } catch (error) {
    } finally {
      commit("setLoading", false);
    }
  },

  async updateNote({ commit }, note) {
    commit("setLoading", true);
    try {
      const response = await axios.put(`/api/notes/${note.id}`, note);
      commit("updateNote", response.data);
    } catch (error) {
    } finally {
      commit("setLoading", false);
    }
  },

  async deleteNote({ commit }, noteId) {
    commit("setLoading", true);
    try {
      await axios.delete(`/api/notes/${noteId}`);
      commit("deleteNote", noteId);
    } catch (error) {
    } finally {
      commit("setLoading", false);
    }
  },
};

const getters = {
  notes: (state) => state.notes,
  note: (state) => state.note,
  isLoading: (state) => state.isLoading,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
