import axios from "../../axios";

const state = {
  modelInfo: "",
  localModels: [],
  chatStream: [],
  runningServerMessage: "",
  runningSystemMessage: "",
};

const mutations = {
  setModelInfo(state, modelInfo) {
    state.modelInfo = modelInfo;
  },
  setLocalModels(state, localModels) {
    state.localModels = localModels;
  },
  setRunningServerMessage(state, runningServerMessage) {
    state.runningServerMessage = runningServerMessage;
  },
  setRunningSystemMessage(state, runningSystemMessage) {
    state.runningSystemMessage = runningSystemMessage;
  },
};

const actions = {
  getRunningServerMessage({ commit }) {
    axios
      .get("/api/info/server")
      .then((response) => {
        commit("setRunningServerMessage", response.data);
      })
      .catch((error) => {
        commit("setRunningServerMessage", "Error: " + error);
      });
  },

  getRunningSystemMessage({ commit }) {
    axios
      .get("/api/info/system")
      .then((response) => {
        commit("setRunningSystemMessage", response.data);
      })
      .catch((error) => {
        commit("setRunningSystemMessage", "Error: " + error);
      });
  },
  // Get Model Info
  getModelInfo({ commit }, payload) {
    if (payload) {
      axios
        .post("/api/show", {
          name: payload,
        })
        .then((response) => {
          commit("setModelInfo", response.data);
        })
        .catch((error) => {
          commit("setModelInfo", error);
        });
    }
  },

  // list Local Models
  getLocalModels({ commit }) {
    axios
      .get("/api/tags")
      .then((response) => {
        commit("setLocalModels", response.data);
      })
      .catch((error) => {
        // commit error message
        commit("setLocalModels", error);
      });
  },
};

const getters = {
  getModelInfo: (state) => state.modelInfo,
  getLocalModels: (state) => state.localModels,
  getRunningServerMessage: (state) => state.runningServerMessage,
  getRunningSystemMessage: (state) => state.runningSystemMessage,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
