const express = require("express");
const router = express.Router();

const modelRoutesController = require("../controllers/modelRoutesController");

// generate single completion
router.post("/create", modelRoutesController.createFromTemplate);

router.post("/delete", modelRoutesController.deleteModel);

router.post("/pull", modelRoutesController.pullModel);

module.exports = router;
