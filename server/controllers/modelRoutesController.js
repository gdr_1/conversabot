const axios = require("../axios");
const config = require("../config");
const { TextDecoder } = require("util");
const logger = require("../logger");

const createFromTemplate = (req, res) => {
  const { name, modelfile } = req.body;

  try {
    axios
      .post(`${config.llmBaseURL}/api/create`, {
        name: name,
        modelfile: modelfile,
      })
      .then((response) => {
        return res.status(200).json(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        return res.status(500).json(error);
      });
  } catch (error) {
    console.error("Unexpected error:", error);
    return res.status(500).json(error);
  }
};

const deleteModel = (req, res) => {
  const { name } = req.body;
  try {
    axios
      .delete(`${config.llmBaseURL}/api/delete`, {
        data: {
          name: name,
        },
      })
      .then((response) => {
        return res.status(200).json(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        return res.status(500).json(error);
      });
  } catch (error) {
    console.error("Unexpected error:", error);
    return res.status(500).json(error);
  }
};

const pullModel = (req, res) => {
  try {
    // Set headers for SSE
    res.writeHead(200, {
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-cache",
      Connection: "keep-alive",
    });

    axios
      .post(`${config.llmBaseURL}/api/pull`, req.body, {
        responseType: "stream",
      })
      .then((response) => {
        // declare variable to set stream date time
        const startTime = new Date().toISOString();

        // variable to store store count of stream data
        let count = 0;

        logger.info("Pull request download stream started");
        const decoder = new TextDecoder("utf-8");

        response.data.on("data", (chunk) => {
          const textChunk = decoder.decode(chunk, { stream: true });
          // Send each JSON object as an SSE message
          res.write(`${textChunk}`);
          count += 1;
        });

        response.data.on("end", () => {
          // end time of the stream
          const endTime = new Date().toISOString();
          // difference between start and end time
          const timeDiff = new Date(endTime) - new Date(startTime);
          const timeDiffSeconds =
            timeDiff > 1000 ? `${timeDiff / 1000} seconds` : `${timeDiff} ms`;

          // log time diff in seconds and count of stream data
          logger.info(
            `Chat request stream ended after ${timeDiffSeconds} with ${count} stream data`
          );

          res.end(); // Close the SSE connection when the stream ends
        });
      })
      .catch((error) => {
        res.write(`event: error\ndata: ${JSON.stringify(error.message)}\n\n`);
        res.end();
      });
  } catch (error) {
    res.write(`event: error\ndata: ${JSON.stringify(error.message)}\n\n`);
    res.end();
  }
};

module.exports = {
  createFromTemplate,
  deleteModel,
  pullModel,
};
