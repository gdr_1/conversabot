// store/index.js
import { createStore } from "vuex";
import chat from "./modules/chat.js";
import info from "./modules/info.js";
import initialModel from "./modules/initialModel.js";
import notes from "./modules/notes.js";
import modelMgmt from "./modules/modelMgmt.js";

// Create a new store instance.
const store = createStore({
  modules: {
    chat,
    info,
    initialModel,
    notes,
    modelMgmt,
  },
});

export default store;
