const winston = require("winston");

const logger = winston.createLogger({
  level: "info", // Minimum level of messages to log
  format: winston.format.combine(
    winston.format.timestamp(), // Add a timestamp
    winston.format.json() // Use JSON format
  ),
  transports: [
    new winston.transports.Console(), // Log to console
    new winston.transports.File({ filename: "combined.log" }), // Log to a file
  ],
});

module.exports = logger;
