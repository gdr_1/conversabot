// create a mixin object
const helpersMixin = {
  data() {
    return {
      alertPopMessage: "",
      isVisible: false,
    };
  },

  methods: {
    alertPopMessageHandler(message) {
      this.alertPopMessage = message;
      this.isVisible = true;
      setTimeout(() => {
        this.alertPopMessage = "";
        this.isVisible = false;
      }, 800);
    },
  },
};

export default helpersMixin;
