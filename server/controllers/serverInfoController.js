const logger = require("../logger");

// import os
const os = require("os");

const getServerInfo = (req, res) => {
  try {
    // return response with status and message
    logger.info("Server is running");
    return res.status(200).json("Server is running");
  } catch (error) {
    // return error response
    // logger.error("Error in getServerInfo", error);
    next(error);
  }
};

const getSystemInfo = (req, res) => {
  try {
    // total memory in gb
    const totalMemoryGB = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2);
    // free memory in gb
    const freeMemoryGB = (os.freemem() / 1024 / 1024 / 1024).toFixed(2);

    // get the system information
    const systemInfo = {
      totalMemory: totalMemoryGB,
      freeMemory: freeMemoryGB,
    };
    // return response with status and system information
    return res.status(200).json(systemInfo);
  } catch (error) {
    // return error response
    // logger.error("Error in getSystemInfo", error);
    next(error);
  }
};

module.exports = {
  getServerInfo,
  getSystemInfo,
};
