// Import environment variables
const env = process.env;

const getEnv = (key, defaultValue = null) => {
  return env[key] || defaultValue;
};

// Get protocol from the environment variables
const protocol = getEnv("PROTOCOL", "http://");

// Get server port and host from the environment variables
const port = getEnv("PORT", 3000);
const host = getEnv("HOST", "localhost");

// base url
const baseURL = `http://${host}:${port}`;

// Get llm  port and host from the environment variables
const llmPort = getEnv("LLM_PORT", 11434);
const llmHost = getEnv("LLM_HOST", "localhost");

// llm base url
const llmBaseURL = `http://${llmHost}:${llmPort}`;

// client port
const clientPort = getEnv("CLIENT_PORT", 8080);
const clientHost = getEnv("HOST", "localhost");

// client base url
const clientBaseURL = `http://${clientHost}:${clientPort}`;

// const HOST_FRONTENT_DEVICE = getEnv("HOST_FRONTENT_DEVICE", "localhost");
const IP_ADDRESSES = getEnv("IP_ADDRESSES", "localhost");

// create url path for client base url for cors
let url = "";
if (IP_ADDRESSES !== "localhost") {
  let ip_addresses = JSON.parse(IP_ADDRESSES);
  for (let i = 0; i < ip_addresses.length; i++) {
    ip_addresses[i] = `${protocol}${ip_addresses[i]}:${clientPort}`;
  }

  url = ip_addresses.join(",");
}

const config = {
  port,
  baseURL,
  llmBaseURL,
  clientBaseURL,
  clientPort,
  url,
};

module.exports = config;
