const { createClient } = require("redis");

const client = createClient({
  url: "redis://redis:6379",
});

client.on("error", (err) => console.error("Redis Client Error", err));

async function connectRedis() {
  try {
    await client.connect();
    console.log("Connected to Redis");
  } catch (error) {
    console.error("Failed to connect to Redis:", error);
    process.exit(1);
  }
}

module.exports = { client, connectRedis };
