const express = require("express");
const router = express.Router();

const {
  saveInitialModel,
  getInitialModel,
  removeInitialModel,
  removeAllInitialModels,
} = require("../controllers/initialModelController");

router.post("/", saveInitialModel);
router.get("/", getInitialModel);
router.delete("/:id", removeInitialModel);
router.delete("/remove-all", removeAllInitialModels);

module.exports = router;
