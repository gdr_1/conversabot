const axios = require("axios");
const config = require("./config");
const axiosInstance = axios.create({
  baseURL: config.llmBaseURL,
});

module.exports = axiosInstance;
