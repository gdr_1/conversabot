// Importing the entire process.env object to make all environment variables available
const env = import.meta.env;

// A helper function to get environment variables, providing a default value if the variable is not set
function getEnvVariable(key, defaultValue = "") {
  return env[key] ?? defaultValue;
}

const host = getEnvVariable("VITE_APP_HOST", "localhost");
const port = getEnvVariable("VITE_APP_PORT", 5000);

const apiUrl = `http://${host}:${port}`;

export default {
  apiUrl,
};
