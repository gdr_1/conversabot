const express = require("express");
const router = express.Router();

// define the server info route from serverInfoController
const serverInfoController = require("../controllers/serverInfoController");

// define the server info route
router.get("/server", serverInfoController.getServerInfo);
router.get("/system", serverInfoController.getSystemInfo);

module.exports = router;
