// use sqlite database
const { db } = require("../database/connection");

const createNote = (req, res) => {
  const { title, content } = req.body;
  const query = `INSERT INTO notes (title, content) VALUES (?, ?)`;

  db.run(query, [title, content], function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      const data = {
        id: this.lastID,
        title: title,
        content: content,
      };
      res.json(data);
    }
  });
};

const getNotes = (req, res) => {
  const query = `SELECT * FROM notes`;

  db.all(query, [], (err, rows) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json({ data: rows });
    }
  });
};

const getNote = (req, res) => {
  const id = req.params.id;
  const query = `SELECT * FROM notes WHERE id = ?`;

  db.get(query, [id], (err, row) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json({ data: row });
    }
  });
};

const updateNote = (req, res) => {
  const id = req.params.id;
  const { title, content } = req.body;
  const query = `UPDATE notes SET title = ?, content = ? WHERE id = ?`;

  db.run(query, [title, content, id], function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json({ success: `Updated item with id ${id}` });
    }
  });
};

const deleteNote = (req, res) => {
  const id = req.params.id;
  const query = `DELETE FROM notes WHERE id = ?`;

  db.run(query, [id], function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json({ success: `Deleted item with id ${id}` });
    }
  });
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
};
