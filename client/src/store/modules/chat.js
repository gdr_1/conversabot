import axios from "../../axios";
import { outputResult } from "../../utils/helpers.js";
import config from "../../config";

const state = {
  completion: "",
  isLoadingCompletion: false,
  chatHistory: [],

  shouldContinue: false,
  chat: [],
  title: "",
  messageArray: [],
  result: "",
  isLoading: false,
  selectedModel: "",
};

const mutations = {
  setChatCompletion(state, completion) {
    state.completion = completion;
  },

  clearChatCompletion(state) {
    state.completion = [];
  },

  setChatHistory(state, chatHistory) {
    state.chatHistory = chatHistory;
  },

  addChatItem(state, chatItem) {
    const chat = {
      id: chatItem.id,
      name: chatItem.name,
      chat: JSON.stringify(chatItem.chat),
      createdAt: chatItem.createdAt,
    };

    state.chatHistory.unshift(chat);
  },

  deleteChatItem(state, itemId) {
    const index = state.chatHistory.findIndex((chat) => chat.id === itemId);
    if (index !== -1) {
      state.chatHistory.splice(index, 1);
    }
  },

  pushChat(state, message) {
    state.chat.push(message);
  },

  clearMessageArray(state) {
    state.messageArray = [];
  },

  setResult(state, result) {
    state.result = result;
  },

  pushMessage(state, message) {
    state.messageArray.push(message);
  },

  setShouldContinue(state, shouldContinue) {
    state.shouldContinue = shouldContinue;
  },

  setIsLoading(state, isLoading) {
    state.isLoading = isLoading;
  },

  // clear chat
  clearChat(state) {
    state.chat = [];
    state.title = "";
  },

  // clear message array
  clearMessageArray(state) {
    state.messageArray = [];
  },

  // clear result
  clearResult(state) {
    state.result = "";
  },

  // setChat

  setChat(state, payload) {
    state.chat = payload.chat;
    state.title = payload.title;
  },

  // set title
  setTitle(state, title) {
    state.title = title;
  },

  currentSelectedModel(state, model) {
    state.selectedModel = model;
  },

  // isLoadingCompletion
  setIsLoadingCompletion(state, isLoading) {
    state.isLoadingCompletion = isLoading;
  },
};

const actions = {
  setSelectedModel({ commit }, model) {
    commit("currentSelectedModel", model);
  },

  chatIsLoading({ commit }, isLoading) {
    commit("setIsLoading", isLoading);
  },

  clearChat({ commit }) {
    commit("clearChat");
  },

  clearMessageArray({ commit }) {
    commit("clearMessageArray");
  },

  clearResult({ commit }) {
    commit("clearResult");
  },

  // clear shouldContinue
  clearShouldContinue({ commit }) {
    commit("setShouldContinue", false);
  },

  setChatFromHistory({ commit }, payload) {
    commit("setChat", payload);
  },

  setTitleClear({ commit }) {
    commit("setTitle", "");
  },

  chatStream({ commit, state }, { prompt }) {
    if (!state.selectedModel) {
      alert("Please select a model first.");
      return;
    }

    commit("setIsLoading", true);
    commit("setShouldContinue", true);
    commit("pushChat", {
      role: "user",
      content: prompt,
    });
    commit("pushChat", {
      role: "assistant",
      content: "",
    });

    const data = {
      model: state.selectedModel,
      messages: state.chat,
    };
    const url = `${config.apiUrl}/api/stream-data`;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        const reader = response.body.getReader();
        const decoder = new TextDecoder("utf-8");
        const readStream = () => {
          if (!state.shouldContinue) {
            reader.cancel();
            commit("clearMessageArray");
            commit("setResult", "");
            commit("setIsLoading", false);
            return;
          }
          reader
            .read()
            .then(({ done, value }) => {
              if (done) {
                commit("setShouldContinue", false);
                commit("clearMessageArray");
                commit("setIsLoading", false);
                return;
              }
              const chunk = decoder.decode(value, { stream: true });
              try {
                const data = JSON.parse(chunk);
                commit("pushMessage", data.message.content);
                state.chat[state.chat.length - 1].content = outputResult(
                  state.messageArray
                );
              } catch (error) {
                console.error("Error parsing JSON:", error);
              }
              readStream();
            })
            .catch((error) => {
              console.error("Error reading the stream:", error);
            });
        };
        readStream();
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  },

  // -------------------------------------

  clearCompletion({ commit }) {
    commit("setChatCompletion", "");
  },

  getChatCompletion({ commit }, payload) {
    commit("setIsLoadingCompletion", true);
    axios
      .post("/api/generate", {
        model: payload.model,
        prompt: payload.prompt,
      })
      .then((response) => {
        commit("setIsLoadingCompletion", false);
        commit("setChatCompletion", response.data.response);
      })
      .catch((error) => {
        commit("setIsLoadingCompletion", false);
        commit("setChatCompletion", error);
      });
  },

  getChatHistory({ commit }) {
    axios
      .get("/api/history/chat-history")
      .then((response) => {
        commit("setChatHistory", response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },

  addChatHistory({ commit }, payload) {
    axios
      .post("/api/history/chat-history", {
        name: payload.name,
        chat: payload.chat,
      })
      .then((response) => {
        // Commit the mutation with the response data
        commit("addChatItem", response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },

  deleteChatHistory({ commit }, payload) {
    axios
      .delete(`/api/history/chat-history/${payload.id}`)
      .then((response) => {
        // Commit the ID of the deleted item to the mutation
        commit("deleteChatItem", payload.id);
      })
      .catch((error) => {
        console.log(error);
      });
  },
};

const getters = {
  getChatCompletion(state) {
    return state.completion;
  },

  getChatHistory(state) {
    return state.chatHistory;
  },

  getChat(state) {
    return state.chat;
  },

  getMessageArray(state) {
    return state.messageArray;
  },

  getResult(state) {
    return state.result;
  },

  getShouldContinue(state) {
    return state.shouldContinue;
  },

  getSelectedModel(state) {
    return state.selectedModel;
  },

  // get title
  getTitle(state) {
    return state.title;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
