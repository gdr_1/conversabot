const express = require("express");
const router = express.Router();

const ollamaRoutesController = require("../controllers/ollamaRoutesController");

// generate single completion
router.post("/generate", ollamaRoutesController.generateCompletion);

router.post("/stream-data", ollamaRoutesController.chatRequest);

router.post("/show", ollamaRoutesController.getModelInfo);

router.get("/tags", ollamaRoutesController.listLocalModels);

module.exports = router;
