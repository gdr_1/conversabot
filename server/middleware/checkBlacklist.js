const { client } = require("../redis/redisClient");

const checkBlacklist = async (req, res, next) => {
  const token = req.headers["authorization"].split(" ")[1];
  try {
    const isBlacklisted = await client.get(token);

    if (isBlacklisted === "blacklisted") {
      return res.status(401).send({ message: "Token is blacklisted." });
    }
    next();
  } catch (error) {
    console.error("Redis error on token check:", error);
    return res.status(500).send({ message: "Error checking token status." });
  }
};

module.exports = checkBlacklist;
