const express = require("express");
const router = express.Router();
const {
  getAllMessages,
  getHistoryById,
  addHistory,
  updateHistory,
  deleteHistory,
} = require("../controllers/messagesHistoryController");

// Set up routes for CRUD operations
router.get("/chat-history", getAllMessages);

router.get("/chat-history/:id", getHistoryById);

router.post("/chat-history", addHistory);

router.put("/chat-history/:id", updateHistory);

router.delete("/chat-history/:id", deleteHistory);

module.exports = router;
