const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

// Import the Redis client to use in the logout controller
const { client } = require("../redis/redisClient");

const {
  registerUser,
  findUser,
  updateUser,
  deleteUser,
  getUsers,
  getUserById,
} = require("../database/connection");

require("dotenv").config();
// Import the SECRET_KEY from the .env file to use in the JWT sign method
const SECRET_KEY = process.env.SECRET_KEY;

// Register user controller
const registerUserController = async (req, res) => {
  const { username, password } = req.body;
  const hashedPassword = await bcrypt.hash(password, 8);

  try {
    await registerUser(username, hashedPassword);
    res.status(201).send({ message: "User created" });
  } catch (error) {
    res.status(500).send({ message: "Username is already taken" });
  }
};

// Find user controller
const loginUserController = async (req, res) => {
  const { username, password } = req.body;
  const user = await findUser(username);

  if (user && (await bcrypt.compare(password, user.password))) {
    const token = jwt.sign({ id: user.id }, SECRET_KEY, { expiresIn: "24h" });
    res.send({ token });
  } else {
    res.status(401).send({ message: "Invalid credentials" });
  }
};

// Update user controller
const updateUserController = async (req, res) => {
  const { newUsername } = req.body;
  try {
    const result = await updateUser(req.userId, newUsername);
    if (result.changed) {
      res.send({ message: "Username updated successfully." });
    } else {
      res.status(404).send({ message: "User not found." });
    }
  } catch (error) {
    res.status(500).send({ message: "Error updating username." });
  }
};

// Delete user controller
const deleteUserController = async (req, res) => {
  try {
    const result = await deleteUser(req.userId);
    if (result.deleted) {
      res.send({ message: "User deleted successfully." });
    } else {
      res.status(404).send({ message: "User not found." });
    }
  } catch (error) {
    res.status(500).send({ message: "Error deleting user." });
  }
};

// Get users controller
const getUsersController = async (req, res) => {
  try {
    const users = await getUsers();
    res.send(users);
  } catch (error) {
    res.status(500).send({ message: "Error getting users." });
  }
};

// Get user by ID controller
const getUserByIdController = async (req, res) => {
  const { id } = req.params;
  try {
    const user = await getUserById(id);
    if (user) {
      res.send(user);
    } else {
      res.status(404).send({ message: "User not found." });
    }
  } catch (error) {
    res.status(500).send({ message: "Error getting user." });
  }
};

// Endpoint to handle user logout
const logoutUserController = async (req, res) => {
  const token = req.headers["authorization"].split(" ")[1];
  const expiration = req.tokenExpDate - Date.now();

  console.log("Attempting to log out");

  // Check if the token is already expired
  if (expiration > 0) {
    try {
      const reply = await client.set(
        token,
        "blacklisted",
        "EX",
        Math.ceil(expiration / 1000)
      );
      console.log("Reply from Redis:", reply);
      res.send({ message: "You have been logged out successfully." });
    } catch (err) {
      console.error("Redis error:", err);
      res.status(500).send({ message: "Logout failed.", error: err.message });
    }
  } else {
    res.status(400).send({
      message: "Token is already expired and does not need to be blacklisted.",
    });
  }
};

module.exports = {
  registerUserController,
  loginUserController,
  updateUserController,
  deleteUserController,
  getUsersController,
  getUserByIdController,
  logoutUserController,
};
