const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/verifyToken");
const checkBlacklist = require("../middleware/checkBlacklist");

const {
  registerUserController,
  loginUserController,
  updateUserController,
  deleteUserController,
  getUsersController,
  getUserByIdController,
  logoutUserController,
} = require("../controllers/authController");

// routes
router.post("/register", registerUserController);
router.post("/login", loginUserController);
router.get("/users", verifyToken, checkBlacklist, getUsersController);
router.get("/user/:id", verifyToken, checkBlacklist, getUserByIdController);
router.put("/update", verifyToken, updateUserController);
router.delete("/delete", verifyToken, deleteUserController);
router.post("/logout", verifyToken, logoutUserController);

module.exports = router;
