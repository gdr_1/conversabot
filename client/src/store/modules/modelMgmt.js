import axios from "../../axios";

import store from "../index.js";

const state = {
  createdModel: null,
};

const mutations = {
  setModelFile(state, createdModel) {
    state.createdModel = createdModel;
  },
};

const actions = {
  createModel({ commit }, payload) {
    axios
      .post("/api/model/create", {
        name: payload.name,
        modelfile: payload.modelfile,
      })
      .then((response) => {
        commit("setModelFile", response.data);
        store.dispatch("info/getLocalModels", null, { root: true });
      })
      .catch((error) => {
        commit("setModelFile", error);
      });
  },
  deleteModel({ commit }, payload) {
    axios
      .post("/api/model/delete", {
        name: payload,
      })
      .then((response) => {
        commit("setModelFile", response.data);
        store.dispatch("info/getLocalModels", null, { root: true });
      })
      .catch((error) => {
        commit("setModelFile", error);
      });
  },
  pullModel({ commit }, payload) {
    fetch("/api/model/pull", {
      method: "POST",
      body: JSON.stringify({
        name: payload,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        commit("setModelFile", response.data);
      })
      .catch((error) => {
        commit("setModelFile", error);
      });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
