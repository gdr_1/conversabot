require("dotenv").config();
const express = require("express");
const cors = require("cors");
const config = require("./config");
const logger = require("./logger");
// const { setupRedis } = require("./get-redis-connection");
const { connectRedis } = require("./redis/redisClient");

// create express app
const app = express();
const corsOptions = {
  origin: function (origin, callback) {
    const allowedOrigins = [
      `http://localhost:${config.clientPort}`,
      config.clientBaseURL,
      config.llmBaseURL,
      config.HOST_FRONTENT_DEVICE,
    ];
    if (!origin || allowedOrigins.indexOf(origin) !== -1) {
      callback(null, true); // No error and origin is allowed
    } else {
      callback(new Error("Not allowed by CORS")); // Origin is not allowed
    }
  },
};

app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Import routes
app.use("/api/info", require("./routes/serverInfoRoutes"));
app.use("/api", require("./routes/ollamaRoutes"));
app.use("/api/history", require("./routes/messagesHistoryRoutes"));
app.use("/api/initial-model", require("./routes/initialModelRoutes"));
app.use("/api/notes", require("./routes/notesRoutes"));
app.use("/api/model", require("./routes/modelRoutes"));

// Centralized Error Handler
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  res.status(statusCode).json({
    success: false,
    error: err.message || "Internal Server Error",
  });
});

app.get("/", (req, res, next) => {
  try {
    res.send("Server is running");
    logger.info("Server is running response sent successfully");
  } catch (error) {
    next(error); // Passes the error to the centralized error handler
  }
});

// setup server port
const port = config.port || 5000;

// setup redis connection
connectRedis()
  .then(() => {
    // run auth routes
    app.use("/api/auth", require("./routes/authRoutes"));

    // run server
    app.listen(port, "0.0.0.0", () => {
      logger.info(`Server is running on port ${port}`);
    });
  })
  .catch((error) => {
    // log error and exit process
    logger.error("Failed to connect to Redis:", error);
    process.exit(1);
  });
