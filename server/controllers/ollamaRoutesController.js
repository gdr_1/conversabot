const axios = require("../axios");
const config = require("../config");
const { TextDecoder } = require("util");
const logger = require("../logger");

const generateCompletion = (req, res) => {
  const { model, prompt } = req.body;
  try {
    try {
      axios
        .post(`${config.llmBaseURL}/api/generate`, {
          model: model,
          prompt: prompt,
          stream: false,
        })

        .then((response) => {
          logger.info("Response from generate completion");
          return res.status(200).json(response.data);
        })
        .catch((error) => {
          // Handle errors from the axios request
          console.error("Error fetching data:", error);
          return res.status(500).json(error);
        });
    } catch (error) {
      // Handle other errors
      console.error("Unexpected error:", error);
      return res.status(500).json(error);
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

const getModelInfo = (req, res) => {
  const { name } = req.body;
  try {
    axios
      .post(`${config.llmBaseURL}/api/show`, {
        name: name,
      })
      .then((response) => {
        return res.status(200).json(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        return res.status(500).json(error);
      });
  } catch (error) {
    console.error("Unexpected error:", error);
    return res.status(500).json(error);
  }
};

const listLocalModels = (req, res) => {
  try {
    axios
      .get(`${config.llmBaseURL}/api/tags`)
      .then((response) => {
        return res.status(200).json(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
        return res.status(500).json(error);
      });
  } catch (error) {
    console.error("Unexpected error:", error);
    return res.status(500).json(error);
  }
};

const chatRequest = (req, res) => {
  try {
    // Set headers for SSE
    res.writeHead(200, {
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-cache",
      Connection: "keep-alive",
    });

    const requestData = req.body; // Data from the incoming POST request

    axios
      .post(`${config.llmBaseURL}/api/chat`, requestData, {
        responseType: "stream",
      })
      .then((response) => {
        // declare variable to set stream date time
        const startTime = new Date().toISOString();

        // variable to store store count of stream data
        let count = 0;

        logger.info("Chat request stream started");
        const decoder = new TextDecoder("utf-8");

        response.data.on("data", (chunk) => {
          const textChunk = decoder.decode(chunk, { stream: true });
          // Send each JSON object as an SSE message
          res.write(`${textChunk}`);
          count += 1;
        });

        response.data.on("end", () => {
          // end time of the stream
          const endTime = new Date().toISOString();
          // difference between start and end time
          const timeDiff = new Date(endTime) - new Date(startTime);
          const timeDiffSeconds =
            timeDiff > 1000 ? `${timeDiff / 1000} seconds` : `${timeDiff} ms`;

          // log time diff in seconds and count of stream data
          logger.info(
            `Chat request stream ended after ${timeDiffSeconds} with ${count} stream data`
          );

          res.end(); // Close the SSE connection when the stream ends
        });
      })
      .catch((error) => {
        res.write(`event: error\ndata: ${JSON.stringify(error.message)}\n\n`);
        res.end();
      });
  } catch (error) {
    res.write(`event: error\ndata: ${JSON.stringify(error.message)}\n\n`);
    res.end();
  }
};

module.exports = {
  generateCompletion,
  getModelInfo,
  listLocalModels,
  chatRequest,
};
