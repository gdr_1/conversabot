import axios from "../../axios";

const state = {
  initialModel: [],
};

const mutations = {
  setInitialModel(state, initialModel) {
    state.initialModel = initialModel;
  },

  setAddInitialModel(state, initialModel) {
    state.initialModel.push(initialModel);
  },
};

const actions = {
  // Get Initial Chat
  getInitialModel({ commit }) {
    axios
      .get("/api/initial-model")
      .then((response) => {
        commit("setInitialModel", response.data);
      })
      .catch((error) => {
        commit("setInitialModel", error);
      });
  },

  // Add Initial Chat
  addInitialModel({ commit }, { model }) {
    axios
      .post("/api/initial-model", {
        model: model,
      })
      .then((response) => {
        commit("setAddInitialModel", response.data);
      })
      .catch((error) => {
        commit("setAddInitialModel", error);
      });
  },
};

const getters = {
  getInitialModel(state) {
    return state.initialModel;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
