import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// Corrected vite.config.js
export default defineConfig({
  plugins: [vue()],
  server: {
    host: "0.0.0.0", // Listen on all network interfaces
  },
});
