// Connect to SQLite database
const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("./database/database.db", (err) => {
  if (err) {
    console.error(err.message);
  } else {
    console.log("Connected to the SQlite database!");
  }
});

db.serialize(() => {
  db.run(`
    CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      username TEXT UNIQUE,
      password TEXT
    )
  `);
});

function registerUser(username, password) {
  return new Promise((resolve, reject) => {
    db.run(
      "INSERT INTO users (username, password) VALUES (?, ?)",
      [username, password],
      function (err) {
        if (err) reject(err);
        else resolve(this.lastID);
      }
    );
  });
}

function findUser(username) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE username = ?", [username], (err, row) => {
      if (err) reject(err);
      else resolve(row);
    });
  });
}

function updateUser(id, newUsername) {
  return new Promise((resolve, reject) => {
    db.run(
      "UPDATE users SET username = ? WHERE id = ?",
      [newUsername, id],
      function (err) {
        if (err) reject(err);
        else resolve({ id: this.lastID, changed: this.changes });
      }
    );
  });
}

function deleteUser(id) {
  return new Promise((resolve, reject) => {
    db.run("DELETE FROM users WHERE id = ?", [id], function (err) {
      if (err) reject(err);
      else resolve({ deleted: this.changes });
    });
  });
}

// getUsers function
function getUsers() {
  return new Promise((resolve, reject) => {
    db.all("SELECT * FROM users", (err, rows) => {
      if (err) reject(err);
      else resolve(rows);
    });
  });
}

// getUserById function
function getUserById(id) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE id = ?", [id], (err, row) => {
      if (err) reject(err);
      else resolve(row);
    });
  });
}

// Create the history table with id and name columns and createdAT column
db.run(
  `CREATE TABLE IF NOT EXISTS history (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    chat TEXT NOT NULL,
    createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
  );`,

  (err) => {
    if (err) {
      console.error(err.message);
    } else {
      console.log("history table created successfully");
    }
  }
);

module.exports = {
  db,
  registerUser,
  findUser,
  updateUser,
  deleteUser,
  getUsers,
  getUserById,
};
