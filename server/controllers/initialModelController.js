const { db } = require("./../database/connection");
const logger = require("../logger");

// save initial model
const saveInitialModel = async (req, res) => {
  // Insert a new item into the database
  const model = req.body.model;
  const query = `INSERT INTO initialModel (model) VALUES (?)`;

  db.run(query, [model], function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      // this.lastID holds the last inserted row ID
      const insertedId = this.lastID;
      const data = {
        id: insertedId,
        model: model,
      };

      // Returning the original request body and the new ID
      res.json(data);
    }
  });
};

// remove initial model
const removeInitialModel = async (req, res) => {
  const id = req.params.id;

  // if id === 'all' then remove all initial models
  if (id === "remove-all") {
    removeAllInitialModels(req, res);
    return;
  }
  const query = `DELETE FROM initialModel WHERE id = ?`;

  db.run(query, [id], function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json({ success: `Deleted item with id ${id}` });
    }
  });
};

// remove all initial models
const removeAllInitialModels = async (req, res) => {
  const query = `DELETE FROM initialModel`;

  db.run(query, function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      res.json({ success: `Deleted all items` });
    }
  });
};

const getInitialModel = async (req, res) => {
  // Query the database for all history

  // if initialModel table does not exist, create it
  db.run(
    `CREATE TABLE IF NOT EXISTS initialModel (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      model TEXT NOT NULL
    );`,
    (err) => {
      if (err) {
        logger.error(err.message);
      } else {
        logger.info("initialModel table created successfully");
      }
    }
  );

  db.all(`SELECT * FROM initialModel ORDER BY id`, (err, rows) => {
    if (err) {
      logger.error(err.message);
    } else {
      res.json(rows);
    }
  });
};

module.exports = {
  saveInitialModel,
  getInitialModel,
  removeInitialModel,
  removeAllInitialModels,
};
