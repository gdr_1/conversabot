// import express router

const express = require("express");
const router = express.Router();

// use sqlite database
const { db } = require("../database/connection");

// import logger
const logger = require("../logger");

// import notes controller
const notesController = require("../controllers/notesController");

// if notes table does not exist, create it
db.run(
  `CREATE TABLE IF NOT EXISTS notes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT,
    content TEXT
  )`,
  (err) => {
    if (err) {
      logger.error(err.message);
    } else {
      logger.info("Table created successfully");
    }
  }
);

// define the notes routes
router.post("/", notesController.createNote);
router.get("/", notesController.getNotes);
router.get("/:id", notesController.getNote);
router.put("/:id", notesController.updateNote);
router.delete("/:id", notesController.deleteNote);

module.exports = router;
