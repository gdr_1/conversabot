# Conversabot Fullstack App

Welcome to Conversabot, a fullstack application built using Vue.js and Node.js (Express) with Docker containers for easy deployment. This document provides an overview of how to use the app, including setting up models, interacting with AI models, and managing chat history.

## Setting Up the App

- **Frontend and Backend Logic**: The frontend and backend logic are separated into different projects for better structure and organization.
- **Docker Containers**: Added Docker containers for both the frontend and backend using Dockerfiles. You can simply run `docker compose build` to build the containers and `docker compose up` to start the app.
- **OLLAMA Models**: After creating the containers, open a terminal and check if Ollama is running by typing `ollama list`. To install a model, simply run `ollama run <model_name>` or find a different model on https://ollama.com/library. Make sure to list Ollama models again to see if the installed model is available.

## Running the App

- **Running the Web App**: Open your browser and type `localhost:5173` to access the running web app.
- **Selecting a Model**: At the top of the main chat window, you should see the name of the model. Select it and start communicating with the AI model by writing prompts.

## Features

- **Server is running** Upon loading the page, you should see a left sidebar with the text “Server is running”. Additionally, you will be able to view your total RAM memory and how much of it is currently being used.
- **Chat History**: The left side navigation panel allows you to save your conversation history. Clicking the floppy disk icon in the right-top corner of the chat window will save your current conversation.
- **Settings**: In the top-left corner, click the gear icon to access settings, where you can create custom models based on your descriptions and how they should act.
- **Notes Model**: The Notes model has all CRUD features. After creating a note, you can delete or copy the text as markdown output. Auto-save functionality developed using Vue.js events allow easy note creation.
- **Ask AI**: For notes, you can write text as markdown and ask the AI to summarize it or provide answers. This feature allows you to create complex notes.

## Custom Model Creation

- **Create Your Own Models**: The custom model creation feature allows you to develop your preferred models, acting like developers, scientists, engineers, and more.
- **Editing Notes**: To edit a note, simply double-click on it.

## Future Development

- **Login System**: currently developing the backend part of the login system with an authentication mechanism.
- **Redis Installation**: The Redis installation in Docker allows for token blacklisting.

* **LangChain Integration** - Integrate LangChain to provide more advanced AI models and features.Enhance text-based interactions with more accurate and relevant responses.
* **Rag** - Integrate RAG with file handling mechanisms

**Troubleshooting**

If you encounter any issues or have questions, please refer to our FAQs section.

Hope you enjoy using Conversabot web app!

Server .env file example:

```# provide the port number of the server
PORT=3000
LLM_PORT=11434
CLIENT_PORT=5173

# protocol used for the server
PROTOCOL=http//

# provide the IP address of the machine where the server is running or localhost
HOST=host

# additional ip addresses for the frontend hosted apart from the server
IP_ADDRESSES=[""]
LLM_HOST=ollama-container

# jwt secret key
SECRET_KEY=secret
```

Frontend .env file example:

```
# provide the host where the client is running localhost or ip address to access from other devices
VITE_APP_HOST=localhost

# provide the port number where server is running for fetching data
VITE_APP_PORT=3000
```
