const jwt = require("jsonwebtoken");
// logger
const { logger } = require("../logger");

require("dotenv").config();
const SECRET_KEY = process.env.SECRET_KEY;

// Middleware to verify token
function verifyToken(req, res, next) {
  const header = req.headers["authorization"];
  if (!header) {
    return res.status(403).send({ message: "No token provided." });
  }

  // Split the header on the space to separate "Bearer" from the "<token>"
  const parts = header.split(" ");
  if (parts.length !== 2 || parts[0] !== "Bearer") {
    return res.status(403).send({ message: "Token format is invalid." });
  }

  const token = parts[1];
  jwt.verify(token, SECRET_KEY, (err, decoded) => {
    if (err) {
      logger.error(`Error name: ${err.name}`);
      logger.error(`Error message: ${err.message}`);
      return res.status(500).send({ message: "Failed to authenticate token." });
    }
    req.userId = decoded.id;
    req.tokenExpDate = decoded.exp * 1000; // JWT exp is in seconds, convert it to milliseconds
    next();
  });
}

module.exports = verifyToken;
