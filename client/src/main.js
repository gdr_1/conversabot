import { createApp } from "vue";
import App from "./App.vue";
import "bootstrap/dist/js/bootstrap.min.js";
import "./styles/app.scss";
import store from "./store";
import axios from "./axios"; // Import your axios instance

import ModalManager from "./components/shared/modal/modalManager";
import Notes from "./components/shared/global/notes/Notes.vue";
import ModelMgmt from "./components/shared/global/modelsMgmt/ModelMgmt.vue";

const app = createApp(App);

// Register global components

// provide axios globally
app.config.globalProperties.$axios = axios;

app.use(store);

app.use(ModalManager);
app.component("Notes", Notes);
app.component("ModelMgmt", ModelMgmt);

app.mount("#app");
