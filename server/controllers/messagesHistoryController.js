const { db } = require("./../database/connection");
const moment = require("moment");
// get all history

const getAllMessages = async (req, res) => {
  // Query the database for all history
  db.all(`SELECT * FROM history ORDER BY id DESC`, (err, rows) => {
    if (err) {
      console.error(err.message);
    } else {
      res.json(rows);
    }
  });
};

// get history by id
const getHistoryById = async (req, res) => {
  const id = req.params.id;
  // base by id select query from history table name and createdAT
  const query = `SELECT * FROM history WHERE id=${id}`;

  // Query the database for all history
  db.all(query, (err, rows) => {
    if (err) {
      console.error(err.message);
    } else {
      res.json(rows);
    }
  });
};

// add history
const addHistory = async (req, res) => {
  // Insert a new item into the database

  // based on the above table structure, we need to insert a name
  // moment current time
  const createdAt = moment().format("YYYY-MM-DD HH:mm:ss");
  const name = req.body.name;
  // stringified chat
  const chat = JSON.stringify(req.body.chat);
  const query = `INSERT INTO history (name, chat, createdAt) VALUES (?, ?, ?)`;

  db.run(query, [name, chat, createdAt], function (err) {
    if (err) {
      res.status(500).json({ error: err.message });
    } else {
      // this.lastID holds the last inserted row ID
      const insertedId = this.lastID;
      const data = {
        id: insertedId,
        name: name,
        chat: req.body.chat,
        createdAt: createdAt,
      };

      // Returning the original request body and the new ID
      res.json(data);
    }
  });
};

// update history
const updateHistory = async (req, res) => {
  // also update time

  const id = req.params.id;
  const name = req.body.name;
  const chat = JSON.stringify(req.body.chat);
  const createdAt = moment().format("YYYY-MM-DD HH:mm:ss");
  const query = `UPDATE history SET name=?, chat=?, createdAt=? WHERE id=${id}`;

  db.run(query, [name, chat, createdAt], (err) => {
    if (err) {
      console.error(err.message);
    } else {
      res.json({ message: "History updated successfully!" });
    }
  });
};

// delete history
const deleteHistory = async (req, res) => {
  // Delete a item from the database
  const id = req.params.id;
  db.run(`DELETE FROM history WHERE id=${id}`, (err) => {
    if (err) {
      console.error(err.message);
    } else {
      res.json({ message: "History deleted successfully!" });
    }
  });
};

module.exports = {
  getAllMessages,
  getHistoryById,
  addHistory,
  updateHistory,
  deleteHistory,
};
