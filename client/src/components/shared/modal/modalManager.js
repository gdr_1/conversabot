import { reactive } from "vue";

export default {
  install(app) {
    const ModalStore = reactive({
      isVisible: false,
      content: null,
      show(component) {
        this.content = component;
        this.isVisible = true;
      },
      hide() {
        this.content = null;
        this.isVisible = false;
      },
    });

    app.config.globalProperties.$modal = ModalStore;
  },
};
